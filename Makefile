# Copyright (C) 2018-2021, 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libsenc2d.a
LIBNAME_SHARED = libsenc2d.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC =\
 src/senc2d_descriptor.c\
 src/senc2d_device.c\
 src/senc2d_enclosure.c\
 src/senc2d_scene.c\
 src/senc2d_scene_analyze.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then \
	     echo "$(LIBNAME)"; \
	   else \
	     echo "$(LIBNAME_SHARED)"; \
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(DPDC_LIBS)

$(LIBNAME_STATIC): libsenc2d.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libsenc2d.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(S2D_VERSION) s2d; then \
	  echo "s2d $(S2D_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -DSENC2D_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@S2D_VERSION@#$(S2D_VERSION)#g'\
	    senc2d.pc.in > senc2d.pc

senc2d-local.pc: senc2d.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@S2D_VERSION@#$(S2D_VERSION)#g'\
	    senc2d.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" senc2d.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/star" \
	src/senc2d.h src/senc2d_sXd_helper.h src/sencX2d.h src/sencX2d_undefs.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/star-enclosures-2d" \
	COPYING README.md

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/senc2d.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-enclosures-2d/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-enclosures-2d/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/senc2d.h"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/senc2d_sXd_helper.h"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/sencX2d.h"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/sencX2d_undefs.h"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(TEST_OBJ_SSP) $(LIBNAME)
	rm -f .config .test libsenc2d.o senc2d.pc senc2d-local.pc

distclean: clean
	rm -f $(DEP) $(TEST_DEP) $(TEST_DEP_SSP)

lint:
	shellcheck -o all make.sh

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_senc2d_square_behind_square.c\
 src/test_senc2d_square_in_square.c\
 src/test_senc2d_square_on_square.c\
 src/test_senc2d_device.c\
 src/test_senc2d_enclosure.c\
 src/test_senc2d_inconsistant_square.c\
 src/test_senc2d_invalid_scenes.c\
 src/test_senc2d_many_enclosures.c\
 src/test_senc2d_many_segments.c\
 src/test_senc2d_multi_media.c\
 src/test_senc2d_scene.c\
 src/test_senc2d_some_enclosures.c\
 src/test_senc2d_some_segments.c\
 src/test_senc2d_unspecified_medium.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

# Tests that require Star-SP
TEST_SRC_SSP = src/test_senc2d_sample_enclosure.c
TEST_OBJ_SSP = $(TEST_SRC_SSP:.c=.o)
TEST_DEP_SSP = $(TEST_SRC_SSP:.c=.d)
SSP_FOUND = $(PKG_CONFIG) --atleast-version $(SSP_VERSION) star-sp

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
SENC2D_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags senc2d-local.pc)
SENC2D_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs senc2d-local.pc)

build_tests: build_library $(TEST_DEP) .test
	@if $(SSP_FOUND); then $(MAKE) $(TEST_DEP_SSP); fi; \
	$(MAKE) -fMakefile -f.test \
	$$(for i in $(TEST_DEP); do echo -f"$${i}"; done) \
	$$(if $(SSP_FOUND); then \
	     for i in $(TEST_DEP_SSP); do echo -f"$${i}"; done; \
	   fi) \
	test_bin

test: build_tests
	@$(SHELL) make.sh run_test $(TEST_SRC)
	@if $(SSP_FOUND); then $(SHELL) make.sh run_test $(TEST_SRC_SSP); fi

.test: Makefile
	@{ $(SHELL) make.sh config_test $(TEST_SRC) ; \
	   if $(SSP_FOUND); then $(SHELL) make.sh config_test $(TEST_SRC_SSP); fi } \
	> $@

clean_test:
	@$(SHELL) make.sh clean_test $(TEST_SRC) $(TEST_SRC_SSP)

$(TEST_DEP): config.mk senc2d-local.pc
	@$(CC) $(CFLAGS_EXE) $(SENC2D_CFLAGS) $(RSYS_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_DEP_SSP): config.mk senc2d-local.pc
	@$(CC) $(CFLAGS_EXE) $(SENC2D_CFLAGS) $(RSYS_CFLAGS) $(SSP_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_OBJ): config.mk senc2d-local.pc
	$(CC) $(CFLAGS_EXE) $(SENC2D_CFLAGS) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

$(TEST_OBJ_SSP): config.mk senc2d-local.pc
	$(CC) $(CFLAGS_EXE) $(SENC2D_CFLAGS) $(RSYS_CFLAGS) $(SSP_CFLAGS) -c $(@:.o=.c) -o $@

test_senc2d_square_behind_square \
test_senc2d_square_in_square \
test_senc2d_square_on_square \
test_senc2d_device \
test_senc2d_inconsistant_square \
test_senc2d_invalid_scenes \
test_senc2d_multi_media \
test_senc2d_scene \
test_senc2d_some_enclosures \
test_senc2d_some_segments \
test_senc2d_unspecified_medium \
: config.mk senc2d-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(SENC2D_LIBS) $(RSYS_LIBS)

test_senc2d_many_enclosures test_senc2d_many_segments: config.mk senc2d-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(SENC2D_LIBS) $(RSYS_LIBS) -lm

test_senc2d_enclosure: config.mk senc2d-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(SENC2D_LIBS) $(RSYS_LIBS) $(S2D_LIBS)

test_senc2d_sample_enclosure: config.mk senc2d-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(SENC2D_LIBS) $(RSYS_LIBS) $(S2D_LIBS) $(SSP_LIBS)
