/* Copyright (C) 2018-2021, 2023, 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SENC_X_2D_H
#error "The sencX2d.h file must be included priorly to this file."
#endif

/* Star-enclosures-XD macros generic to the SENCXD_DIM */
#undef SENCXD
#undef sencXd
#undef SENCXD_

/* Function names that require additional dedicated macros */
#undef senc2d_scene_get_primitives_count
#undef senc2d_scene_get_primitive
#undef senc2d_scene_get_primitive_media
#undef senc2d_scene_get_primitive_enclosures
#undef senc2d_enclosure_get_primitive
#undef senc2d_enclosure_get_primitive_id

#undef SENC_X_2D_H
